FROM nfnty/arch-mini

LABEL maintainer="Jason Ozias <jason.g.ozias@gmail.com>"

ENV PATH $PATH:/opt/atlas

# Setup Arch
COPY config/arch/mirrorlist /etc/pacman.d/mirrorlist
COPY config/arch/pacman.conf /etc/pacman.conf
RUN pacman -Syu --noconfirm && pacman -S libaio unzip --noconfirm

# Setup the Oracle Instant Client
RUN mkdir -p /opt/oracle
WORKDIR /opt/oracle
RUN curl -o ic-b.zip https://s3.us-east-2.amazonaws.com/atlas-instantclient/instantclient-basic-linux.x64-12.2.0.1.0.zip && \
unzip ic-b.zip && \
rm ic-b.zip && \
ln -s libclntsh.so.12.1 libclntsh.so && \
sh -c "echo /opt/oracle/instantclient_12_2 > /etc/ld.so.conf.d/oracle-instantclient.conf" && \
ldconfig && \
pacman -Rsc unzip --noconfirm

# Setup atlas
RUN mkdir -p /opt/atlas/env
WORKDIR /opt/atlas

# Run Atlas on startup
CMD [ "atlas-api", "-vvv" ]